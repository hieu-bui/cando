addpath json2cndo
addpath cndo2atomic_v3.4
addpath topology2pdb_ver3_bulge

%% Generate .cndo file for PDB
json2cndo('tutorial_v2.json', 'tutorial_v2.csv', 'tutorial_v2.cndo', 'honeycomb');
json2cndo('tutorial_v2.json', 'tutorial_v2.csv', 'tutorial_v2.cndo', 'square');
json2cndo('tutorial_v2.json', '', 'tutorial_v2.cndo', 'honeycomb');

json2cndo('rectangle.json', 'rectangle.csv', 'rectangle.cndo', 'square');

json2cndo('dpe.json', 'dpe.csv', 'dpe.cndo', 'square');
json2cndo('duplex32.json', 'duplex32.csv', 'duplex32.cndo', 'square');
%% Generate PDB file for Chimera

param.StrandColor = [190 190 190; 86 180, 233];
param.L_thres = 100;
param.molmapResolution = 3;
param.WindowSize = [640 480];
param.chimeraEXE = '"/Applications/Chimera.app"';
param.chimeraOPTION = '--silent --script';

CAD_path = 'duplex32.cndo';
work_DIR = 'duplex';

CAD_path = 'dpe.cndo';
work_DIR = 'dpe';

CAD_path = 'example_4way_junction.cndo';
work_DIR = 'example_4way_junction';

CAD_path = 'output.cndo';
work_DIR = 'output';

CAD_path = 'rectangle.cndo';
work_DIR = 'rectangle';



main_cndo2pdb(CAD_path, work_DIR, param);